const boxContainer = document.getElementById("container");
const button = document.getElementsByClassName("reset");

const gifs = [
  './gifs/1.gif',
  './gifs/2.gif',
  './gifs/3.gif',
  './gifs/4.gif',
  './gifs/5.gif',
  './gifs/6.gif',
  './gifs/7.gif',
  './gifs/8.gif',
  './gifs/9.gif',
  './gifs/10.gif',
  './gifs/11.gif',
  './gifs/12.gif',
  './gifs/1.gif',
  './gifs/2.gif',
  './gifs/3.gif',
  './gifs/4.gif',
  './gifs/5.gif',
  './gifs/6.gif',
  './gifs/7.gif',
  './gifs/8.gif',
  './gifs/9.gif',
  './gifs/10.gif',
  './gifs/11.gif',
  './gifs/12.gif'
]


function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

function createDivsForColors(gifsArray) {
  for (let i = 0; i < gifsArray.length; i++) {
    // create a new div
    const newDiv = document.createElement("div");
    const frontDiv = document.createElement("div");
    const backDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add('box');
    frontDiv.classList.add('front');
    backDiv.classList.add('back');

    //add img to front and back div
    const image1 = document.createElement('img');
    image1.src = "./gifs/trishul.jpg";
    image1.id = "imgTrishul";
    frontDiv.append(image1);
    const image2 = document.createElement('img');
    image2.src = gifsArray[i];
    image2.id = gifsArray[i];
    // image2.setAttribute("id",imgIdArray[i]);
    backDiv.append(image2);

    // append the div to the element with an id 
    newDiv.append(frontDiv);
    newDiv.append(backDiv);
    boxContainer.append(newDiv);

    // append the div to the element with an id 
    newDiv.append(frontDiv);
    newDiv.append(backDiv);
    boxContainer.append(newDiv);
    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

  }
  let scoreContainer = document.createElement('div');
  scoreContainer.className = "score box";
  boxContainer.insertBefore(scoreContainer, boxContainer.childNodes[12])
}


let shuffledGifs = shuffle(gifs);
createDivsForColors(shuffledGifs);


let clickCount = 0;
let scoreCount = 0;
let matchCount = 0;
let allow = true;
let resetElementsArr = [];
let elementArr1;

let storedScore = localStorage.getItem("bestScore");
if (storedScore === null) {
  storedScore = "I am waiting";
}
document.getElementsByTagName("p")[0].innerText = `Best-Score : ${storedScore}`;
document.getElementsByClassName("score")[0].innerText = "0";


// for checking whether the two boxes firstChild are having the same id 
function check(elementArr1, elementArr2) {

  if (elementArr1[2].firstChild.id !== elementArr2[2].firstChild.id) {
    elementArr1.forEach(element => {
      element.removeAttribute("id");
    })
    elementArr2.forEach(element => {
      element.removeAttribute("id");
    })
    resetElementsArr.length = resetElementsArr.length - 2;
  } else {
    matchCount += 1;
  }
  if (matchCount === 12) {
    let score = localStorage.getItem("bestScore");
    if (scoreCount < Number(score) || score === null) {
      localStorage.setItem("bestScore", `${scoreCount}`)
      document.getElementsByTagName("p")[0].innerText = `Best-Score : ${scoreCount}`
    }
    alert("you won the match!!!");
    scoreCount = 0;
  }
  clickCount = 0;
  allow = true; 
}


function handleCardClick(event) {

  if (event.target.id === "imgTrishul") {
    clickCount += 1;
    let boxElement = event.path[2]
    let frontElement = boxElement.firstChild;
    let backElement = boxElement.lastChild;

    if (boxElement.id !== "boxClicked" && allow) {

      scoreCount += 1;
      document.getElementsByClassName("score")[0].innerText = `${scoreCount}`;

      if (clickCount < 2) {
        boxElement.id = "boxClicked"
        frontElement.id = "frontImg";
        backElement.id = "backImg";
        elementArr1 = [boxElement, frontElement, backElement];
        resetElementsArr.push(elementArr1);

      } else {
        boxElement.id = "boxClicked"
        frontElement.id = "frontImg";
        backElement.id = "backImg";
        let elementArr2 = [boxElement, frontElement, backElement];
        resetElementsArr.push(elementArr2)
        allow = false;
        setTimeout(check, 2000, elementArr1, elementArr2);
      }
    }
  }

}

//for reset

function clear() {
  resetElementsArr.forEach(elementArr => {
    elementArr.forEach(element => {
      element.removeAttribute("id");
    })
  })
  clickCount = 0;
  matchCount = 0;
  scoreCount = 0;
  resetElementsArr.length = 0;
  document.getElementsByClassName("score")[0].innerText = "0";
  allow = true;

}
button[0].addEventListener("click", clear);

